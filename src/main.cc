#include "clock.h"
#include <deque>
#include <iomanip>
#include <iostream>
#include <qlist.h>
#include <qnamespace.h>
#include <string>
#include <thread>

#include "QHotkey"
#include "clock_disp.h"
#include "hotkeys.h"
#include <QtGui>

using std::cout;
using std::string;
using std::chrono::duration_cast;
using us = std::chrono::microseconds;
using ms = std::chrono::milliseconds;
using S = std::chrono::duration<double>;
using M = std::chrono::minutes;
using H = std::chrono::hours;

template <typename dur> int64_t get_micro(dur d) {
  return duration_cast<us>(d).count();
}

template <typename P, typename Dur>
Dur print_dur_part(const Dur &d, const string &post) {
  Dur rem = d;
  if (duration_cast<P>(rem).count() > 0) {
    cout << duration_cast<P>(rem).count() << post;
    rem -= duration_cast<P>(rem);
  }
  return rem;
}

template <typename Dur> void print_dur(const Dur &d) {
  Dur rem = d;
  rem = print_dur_part<H, Dur>(rem, "h ");
  rem = print_dur_part<M, Dur>(rem, "m ");
  S sec = duration_cast<ms>(rem);
  cout << sec.count() << "s\n";
}

void wait(const ms &w) { std::this_thread::sleep_for(w); }

double seconds(const ms &d) { return S{duration_cast<ms>(d)}.count(); }

void test_splits() {
  katana_steel::Clock g{};
  std::deque<ms> splits{};
  g.start();
  wait(ms{1430});
  splits.emplace_back(duration_cast<ms>(g.elapsed()));
  wait(ms{2245});
  splits.emplace_back(duration_cast<ms>(g.elapsed()));
  wait(ms{1793});
  splits.emplace_back(duration_cast<ms>(g.elapsed()));
  wait(ms{840});
  splits.emplace_back(duration_cast<ms>(g.elapsed()));
  ms b{0};
  for (auto &&d : splits) {
    ms sub = d - b;
    b += sub;
    cout << "sub " << seconds(sub) << "s - " << seconds(d) << "s\n";
  }
}

int main(int p1, char **p2) {

  QApplication app{p1, p2};
  katana_steel::ClockDisp w{};
  w.setStyleSheet("color:lime;");
  w.setWindowFlags(Qt::ToolTip | Qt::WindowStaysOnTopHint |
                   Qt::FramelessWindowHint | Qt::X11BypassWindowManagerHint);
  for (auto &&x : QCoreApplication::arguments()) {
    if (x.contains("color=")) {
      w.setStyleSheet("color: " + x.split("=").back() + ";");
    }
    if (x.contains("loc=")) {

      QRect screenrect = app.primaryScreen()->geometry();
      switch (x.split("=").back().toInt()) {
      case 1:
        w.move(screenrect.left(), screenrect.top());
        break;
      case 2:
        w.move(screenrect.right() - w.width(), screenrect.top());
        break;
      }
    }
  }
  QList<QHotkey *> hotkeys{};
  w.show();
  register_hotkeys(&app, &w, hotkeys);
  app.exec();
  for (auto ptr : hotkeys)
    delete ptr;
  return 0;
}