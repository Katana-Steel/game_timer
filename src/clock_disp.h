#pragma once

#include "clock.h"
#include "ui_clock_disp.h"
#include <QtGui>
#include <qevent.h>

namespace katana_steel {
class ClockDisp : public QWidget {
  Ui::Form ui;
  Clock rtc;
  QTimer update;
  void mousePressEvent(QMouseEvent *event) override;
  void keyPressEvent(QKeyEvent *event) override;

private Q_SLOTS:
  void updateDisp();

public:
  ClockDisp(QWidget *parent = nullptr);
  auto elapsed() const { return rtc.elapsed(); };
  auto is_running() const {return rtc.is_running();};
public Q_SLOTS:
  void startTimer();
  void toggleTimer();
  void stopTimer();
};
} // namespace katana_steel
