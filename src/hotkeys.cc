#include "hotkeys.h"
#include "QHotkey"
#include "clock_disp.h"
#include <QtGui>
#include <qlist.h>
#include <qnamespace.h>
#include <qobjectdefs.h>

void register_hotkeys(QApplication *app, katana_steel::ClockDisp *win,
                      QList<QHotkey *> &lst) {
  auto hk1 = new QHotkey{QKeySequence("Alt+Shift+Space"), true, app};
  QObject::connect(hk1, &QHotkey::activated, win, [win]() {
    if (win->is_running())
      win->stopTimer();
    else
      win->startTimer();
  });
  lst.append(hk1);
  hk1 = new QHotkey{QKeySequence("Alt+Shift+Q"), true, app};
  QObject::connect(hk1, &QHotkey::activated, app, [app]() { app->quit(); });
  lst.append(hk1);
}