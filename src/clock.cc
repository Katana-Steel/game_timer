#include "clock.h"

namespace katana_steel {
void Clock::start() {
  running = true;
  current = internal_timer.now();
  dur = current - current;
}

clk::duration Clock::elapsed() const {
  if (!running)
    return dur;
  return dur + (internal_timer.now() - current);
}

void Clock::pause() {
  auto add = internal_timer.now();
  dur += (add - current);
  current = add;
  running = !running;
}

void Clock::resume() {
  running = true;
  current = internal_timer.now();
}

bool Clock::is_running() const { return running; }

elapse_time::H elapse_time::hours() const {
  if (!_hours.has_value())
    _hours = std::chrono::duration_cast<H>(elapse);
  return _hours.value();
}

elapse_time::M elapse_time::minutes() const {
  auto m = elapse - hours();
  if (!_minutes.has_value())
    _minutes = std::chrono::duration_cast<M>(m);
  return _minutes.value();
}

double elapse_time::seconds() const {
  auto ms = (elapse - hours()) - minutes();
  return std::chrono::duration_cast<MS>(ms).count() / 1000.0;
}
} // namespace katana_steel
