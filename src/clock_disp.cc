#include "clock_disp.h"
#include <fmt/format.h>
#include <qevent.h>
#include <qnamespace.h>

namespace katana_steel {
ClockDisp::ClockDisp(QWidget *parent)
    : QWidget(parent), ui{}, rtc{}, update{this} {
  ui.setupUi(this);
  setAttribute(Qt::WA_TranslucentBackground);

  connect(&update, &QTimer::timeout, this,
          QOverload<>::of(&ClockDisp::updateDisp));
  updateDisp();
}

void ClockDisp::updateDisp() {
  auto now = katana_steel::elapse_time{rtc.elapsed()};
  auto hours = now.hours().count();
  auto minutes = now.minutes().count();
  if (hours > 0) {
    ui.hours->setVisible(true);
    ui.hour_spacer->setVisible(true);
    ui.hours->display((int)hours);
  } else {
    ui.hours->setHidden(true);
    ui.hour_spacer->setHidden(true);
  }
  if (minutes > 0) {
    ui.minutes->setVisible(true);
    ui.minut_spacer->setVisible(true);
    ui.minutes->display((int)minutes);
  } else {
    ui.minutes->setHidden(true);
    ui.minut_spacer->setHidden(true);
  }
  ui.seconds->display(fmt::format("{0:0.2f}", now.seconds()).c_str());
}

void ClockDisp::startTimer() {
  rtc.start();
  updateDisp();
  update.start(10);
}

void ClockDisp::stopTimer() {
  rtc.pause();
  updateDisp();
  update.stop();
}

void ClockDisp::mousePressEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    if (rtc.is_running())
      stopTimer();
    else
      startTimer();
  }
  if (event->button() == Qt::RightButton) {
    toggleTimer();
  }
}

void ClockDisp::keyPressEvent(QKeyEvent *event) {
  if (event->modifiers() & (Qt::Modifier::ALT | Qt::Modifier::SHIFT)) {
    if (event->key() == Qt::Key_Q)
      close();

    if (event->key() == Qt::Key_Escape)
      close();

    if (event->key() == Qt::Key_Space) {
      if (rtc.is_running())
        stopTimer();
      else
        startTimer();
    }
    return;
  }
  return QWidget::keyPressEvent(event);
}

void ClockDisp::toggleTimer() {
  if (rtc.is_running())
    rtc.pause();
  else
    rtc.resume();
}

} // namespace katana_steel
