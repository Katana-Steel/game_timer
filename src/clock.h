#pragma once
#include <chrono>
#include <optional>

namespace katana_steel {
template <typename dur> int64_t get_microseconds(const dur &d) {
  return std::chrono::duration_cast<std::chrono::microseconds>(d).count();
}

using clk = std::chrono::high_resolution_clock;

class Clock {
  clk internal_timer;
  clk::time_point current;
  clk::duration dur;
  bool running = false;

public:
  Clock() = default;
  void start();
  clk::duration elapsed() const;
  void pause();
  void resume();
  bool is_running() const;
};

struct elapse_time {
  clk::duration elapse;
  using H = std::chrono::hours;
  using M = std::chrono::minutes;
  using MS = std::chrono::milliseconds;
  mutable std::optional<H> _hours;
  mutable std::optional<M> _minutes;
  H hours() const;
  M minutes() const;
  double seconds() const;
};

} // namespace katana_steel
