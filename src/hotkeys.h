#pragma once

class QApplication;
class QHotkey;
namespace katana_steel {
class ClockDisp;
}

#include <QList>
void register_hotkeys(QApplication *app, katana_steel::ClockDisp *win, QList<QHotkey*> &lst);
