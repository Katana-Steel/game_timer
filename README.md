Game Timer
==========

I lacked a native speedrunning timer under linux so I wrote my own

Hotkeys
=======

* Alt+Shift+Q : Quits the timer
* Alt+Shift+Space : starts and stops the timer

Running
=======

`./timer` start with the timer centered

`./timer loc=1` starts the timer in the top left

`./timer loc=2` starts the timer in the top right

`./timer color="CSS Color"` sets the color of the digits

Building
========

Please ensure to have Qt5 or Qt6 installed
```bash
git clone --recurse-submodules https://gitlab.com/Katana-Steel/game_timer
cd game_timer
cmake -B build -S . -G Ninja
cmake --build build
build/timer loc=2 color=red  # start in the top right with red numbers
```

ToDo
====

* making and displaying splits
* making global hotkeys configurable
* add an OBS overlay recording window
