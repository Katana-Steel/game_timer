#include <iostream>
#include <string>

#include "clock.h"
#include <thread>

using std::cout;
using katana_steel::Clock;
using katana_steel::elapse_time;
using ms = std::chrono::milliseconds;
using fms = std::chrono::duration<double, std::milli>;
using S = std::chrono::duration<double>;

bool is_within_limit(const fms &e, const fms &m, const fms &p)
{
    return (e > (p - m)) && (e < (p + m));
}

void wait(const ms &w)
{
    std::this_thread::sleep_for(w);
}

void print_time(const elapse_time &et)
{
    cout << "hours: " << et.hours().count() << " minutes: " << et.minutes().count() << " seconds: " << et.seconds() << '\n';
}

void test1_create_clock()
{
    cout << "test1: creating clock, ";
    Clock g{};
    cout << "done\n";
}

void test2_start_the_clock()
{
    cout << "test2: starting the clock, ";
    Clock g{};
    g.start();
    if (g.is_running())
        cout << "passed\n";
    else {
        cout << "failed" << std::endl;
        throw new int{2};
    }
}
void test3_get_elapsed_time()
{
    cout << "test3: get elapsed, ";
    ms small_wait{300};
    ms margin{1};
    Clock g{};
    g.start();
    std::this_thread::sleep_for(small_wait);
    fms e = g.elapsed();
    if (is_within_limit(e, margin, small_wait))
        cout << "passed " << e.count() << "ms\n";
    else {
        cout << "failed " << e.count() << "ms" << std::endl;
        throw new int{3};
    }
}

void test4_pause_timer()
{
    cout << "test4: pause after 0.3 sec, ";
    ms small_wait{300};
    ms margin{1};
    Clock g{};
    g.start();
    wait(small_wait);
    g.pause();
    fms b = g.elapsed();
    wait(small_wait);
    fms e = g.elapsed();
    if (is_within_limit(e, margin, small_wait) && b == e)
        cout << "passed " << e.count() << "ms\n";
    else {
        cout << "failed " << b.count() << "ms / " << e.count() << "ms" << std::endl;
        throw new int{4};
    }
}

void test5_getting_splits()
{
    cout << "test5: compare elapse after 2 waits, ";
    ms small_wait{300};
    ms margin{1};
    Clock g{};
    g.start();
    wait(small_wait);
    fms b = g.elapsed();
    wait(small_wait);
    fms e = g.elapsed();
    if (is_within_limit(e, margin, 2*small_wait) && is_within_limit(b, margin, small_wait))
        cout << "passed " << b.count() << "ms -> " << e.count() << "ms\n";
    else {
        cout << "failed " << b.count() << "ms / " << e.count() << "ms" << std::endl;
        throw new int{5};
    }
}

void test6_pause_and_resume()
{
    cout << "test6: compare elapse after pause + resume and 2 waits, ";
    ms small_wait{100};
    ms margin{1};
    Clock g{};
    g.start();
    wait(small_wait);
    g.pause();
    wait(small_wait);
    g.resume(); // resume
    wait(small_wait);
    fms e = g.elapsed();
    if (is_within_limit(e, margin, 2*small_wait))
        cout << "passed " << e.count() << "ms\n";
    else {
        cout << "failed " << e.count() << "ms" << std::endl;
        throw new int{6};
    }
}

void test7_printing_time(Clock &g)
{
    cout << "test7: get elapse after pause resume and 2 waits, with prints after each\n";
    ms small_wait{100};
    wait(small_wait);
    print_time({g.elapsed()});
    g.pause();
    print_time({g.elapsed()});
    wait(small_wait);
    print_time({g.elapsed()});
    g.resume(); // resume
    wait(small_wait);
    print_time({g.elapsed()});
}

int main()
{
    try {
        test1_create_clock();
        Clock g{}; // create a clock as soon as we're sure it's sound
        test2_start_the_clock();
        g.start(); // and start it to better test time elapse and it printing
        test3_get_elapsed_time();
        test4_pause_timer();
        test5_getting_splits();
        test6_pause_and_resume();
        test7_printing_time(g);
    }
    catch (int *t)
    {
        int e = *t;
        cout << "test" << e << " failed\n";
        delete t;
        return 1;
    }
}